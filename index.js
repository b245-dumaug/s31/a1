const http = require('http');
const port = 3000;

const server = http.createServer((req, res) => {
  if (req.url === '/login') {
    res.end('Welcome to the login page.');
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Error: 404 Page Not Found');
  }
});

server.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});


